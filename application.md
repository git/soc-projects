# Google Summer of Code Application

## Why does your org want to participate in Google Summer of Code?

It is a nice opportunity to get young students excited about free software and GNU. To involve them in our ever-growing community and to get some useful code in the bargain.

## How many potential mentors have agreed to mentor this year?

11-15

## How will you keep mentors engaged with their students?

Each of our mentors have been involved with their respective package for a long time. They are also required to appoint to a backup mentor in case they are not available. On top of that, we collect the personal contact details of each of the mentors and can contact them directly in order to ensure that they remain engaged with their students. As a final backup, Jose, Giuseppe and I act as the last layer of backups if things go really wrong (They never have)

## How will you help your students stay on schedule to complete their projects?

We always require our students to have formulated a detailed proposal of their project along with a projected timeline. The mentors look over these to make sure that none of the timelines are overly ambitious or extremely lax. During GSoC, students are expected to check in with their mentors regularly and report on their progress. Mentors will often spend additional time with the student to bring them back on schedule if they get stuck, or are just in need of a little more help.

## How will you keep your students involved with your community during GSoC?
GNU is all about cooperation and sharing. Before and during GSoC, we encourage students to conduct all their discussions on our public channels (Mailing lists, IRC, etc.). This is not always easy since many students are shy or afraid of having technical discussions in the open. We spend time talking to them and helping them overcome this fear. Open discussions allow them to actively engage with the rest of the community as everyone chimes in with new ideas and thoughts. Students across the various projects can also talk and discuss with each other over our IRC channels.

## How will you keep your students involved with your community after GSoC?
We hope that most of our students go on to become regular contributors. Mentoring and help does not start and stop only with GSoC. Our enthusiastic developers are always happy to continue guiding the students into different areas of the codebase and different packages. All students are also invited to our GNU Hackers' Meetings and we try to foster a spirit of community by trying to meet up during other local technical and non-technical events.


# Public Profile

## Name

The GNU Project

## Website URL

https://gnu.org

## Tagline

GNU is a free software Operating System and a collection of other utilities

## Logo

[GNU-Logo.png]

## Primary License

GNU General Public License version 3.0

## Organization Category

Operating Systems

## Technology Tags

C/C++, Python, Shell Script, Javascript, Android

## Topic tags

HTTP, Operating Systems, Kernel, Mobile, Oackage Managers

## Ideas List

https://www.gnu.org/software/soc-projects/ideas.html

## Short Description

GNU is a Unix-like operating system. That means it is a collection of many programs: applications, libraries, developer tools, even games, all licensed under a copyleft license

## Long Description

GNU is a Unix-like operating system. That means it is a collection of many programs: applications, libraries, developer tools, even games. The development of GNU, started in January 1984, is known as the GNU Project. Many of the programs in GNU are released under the auspices of the GNU Project; those we call GNU packages.

## List of Participating GNU packages

The GNU Project acts as an umbrella organization for its various packages. Participating this year are:
* GNU Poke
* GNU Wget / Wget2
* GNU Guix
* GNU Taler
.. and more

See [here](https://www.gnu.org/software/soc-projects/) for information on how to apply for a project with the GNU Project

## Application Template

Please see [here](https://www.gnu.org/software/soc-projects/guidelines.html) for the complete template.

# Name

# Email address

# The name of the project
If your project is from the ideas list, please use the same title, except for prepending the package name.

# Summary
Please include this, rather than just referring to the suggestion, to help avoid misunderstanding.

# Benefits
Please explain how users will benefit from your project. How will the GNU project itself benefit?

# Deliverables
What software will be added or changed? What parts of the project's code will be affected? Which documentation will you update?

# Plan
The total work broken down into weekly or fortnightly chunks.

Remember to mention any periods during the summer when you won't actually be available to work on the project (though remember, the Summer of Code project is expected to be your main activity).

# Communication
Good communication is essential to students' success. Please indicate the ways in which you will contact your mentor (and a schedule for doing it) to ensure that they're always aware of your progress.

# Qualification
Why did this project appeal to you? How will you benefit from it? Why are you particularly suited to work on this? What will you do once the project is "finished"? Have you worked on any Free Software before? Of the skills that you will need to complete the project, which do you already have? What will you need to lean?
