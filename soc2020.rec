# soc2020.rec - Database for the GNU Summer Of Code.
#
#
# This file contains the list of projects/ideas and mentors that are
# part of the GNU application for SoC 2018.
#
# Types of records stored in this file:
#
#     Project -> GNU package participating in the SOC.
#     Mentor  -> Mentor for one or more projects.
#     Student -> Student participating in SOC under GNU.
#
# Useful queries:
#
# - Number of essential slots:
#   recsel soc2020.rec -t Project -p 'Sum(EssentialSlots)'
#
# - Number of desired slots:
#   recsel soc2020.rec -t Project -p 'Sum(DesiredSlots)'
#
# - Desired slots vs. mentored students per project:
#
#   recsel soc2020.rec -t Student -j Project \
#   | recsel -G Project_Name \
#     -p 'Project_Name,Count(Email):Students,Project_DesiredSlots' -U
#
# - Selected students per project
#
#   recsel soc2020.rec -t Student -j Project -e 'Selected = "yes"' \
#   | recsel -G Project_Name -p Email:Student,Project_name:Project
#
#   and number of:
#
#   ... | recsel -p 'Count(Student)'
#
# For inquiries about this file please contact summer-of-code@gnu.org.

%rec: Project
%key: Name
%doc: GNU package contributing ideas for the SOC 2014.
%type: EssentialSlots,DesiredSlots int
%summary: Name
%type: When date
%type: Status enum FOO BAR BAZ

Name: lilypond
EssentialSlots: 1
DesiredSlots: 2

Name: social
EssentialSlots: 3
DesiredSlots: 4

Name: poke
EssentialSlots: 1
DesiredSlots: 1

Name: taler
EssentialSlots: 1
DesiredSlots: 2

Name: emacs
EssentialSlots: 1
DesiredSlots: 1

Name: xaos
EssentialSlots: 1
DesiredSlots: 1

Name: texmacs
EssentialSlots: 1
DesiredSlots: 1

Name: gnuastro
EssentialSlots: 1
DesiredSlots: 1

Name: guix
EssentialSlots: 2
DesiredSlots: 2

Name: wget
EssentialSlots: 1
DesiredSlots: 1

%rec: Mentor
%key: Email
%mandatory: Project
%type: Project rec Project
%confidential: Phone AltEmail

Name: Gábor Boskovits
Email: boskovits@gmail.com
AltEmail: encrypted-1PIEy+umHa6780/vu21XQfOit8hoYhXG36f2JL6esNSMmeocUxMOIpIIzqSuFPqIlkUHEbMZNe5sD+FE9JcxRPaZcxw=
Phone: encrypted-GDXeEEYuZtM5m49pC/h2aUe0tPLdu+oZz+hxXJJbJpfm6kg+
Project: guix

Name: Marcello Stanisci
Email: stanisci.m@gmail.com
AltEmail: encrypted-Ac1rlBI1KTGq8XVp3iBsR38Pb0Y=
Phone: encrypted-GuhgoUImcSElm1eCsVxzU3F93vmnul3ysmMyemEMj5BajARf
Project: taler

Name: Urs Liska
Email: ul@openlilylib.org
AltEmail: encrypted-LGxU/Kd1VKeLwgB7GFcVyZvUbi7tBXNklWwZ1OMaee1yEXF2
Phone: encrypted-wv/CaeOokbgIc6df8PXRGq45BG+Gv5Cqpe2T+AdVgeQzbEWA
Project: lilypond

Name: Mohammad Akhlaghi
Email: mohammad@akhlaghi.org
AltEmail: encrypted-ps6vjd6OwmUi0XO4Yl4SgcyzkH5kN5fA4DKfn4sz6cKNOZ0k
Phone: encrypted-II4rgwrk2N0l4xeubrZbJr6EQpfEHFaARU1FUVvifmsc34zM
Project: gnuastro

Name: Danny Milosavljevic
Email: dannym@scratchpost.org
AltEmail: encrypted-rZFd+lQxOX3qgadGxzhWUT6jGjPeRVcRfHfRUn7V8a8qNaem
Phone: encrypted-kl3cIr8AR3pwm1AU+LD9GpVDhDWh79l/tysnQBWTGRo/cS35
Project: guix

Name: Joshua Judson Rosen
Email: rozzin.gsoc@hackerposse.com
AltEmail: encrypted-SIViGBdUSrzjT+JvSPIfAhgLayYTB3/yOlIn6t3sVpncCm8f
Phone: encrypted-OkYuxpn3GqTSV3scUdw549rpfxqAdu6k67tTWLNaqOvpS9pQ
Project: social

Name: Pjotr Prins
Email: pjotrprins@gmail.com
AltEmail: encrypted-HM0jHfDy+E0KEljPCgy+Y2pCeqx8AZbhknruOp2zgG3yeoaW
Phone: encrypted-alAxtFFPsJ5UhQOnoeDtBC2VGyc=
Project: guix

Name: Christian Grothoff
Email: grothoff@gnu.org
AltEmail: encrypted-8sXBEJjQ+FgtAe2SBbQCb5hJT8uVvLtX4eHyVuP/bOHRAUjG
Phone: encrypted-oPLszrfwhNxTLOV8FIy+0LJB8sc=
Project: gnunet
Project: taler
Project: libmicrohttpd
Project: libextractor

Name: Martin Schanzenbach
Email: mschanzenbach@gmail.com
AltEmail: encrypted-SMZ6CtyYRpmrDWr/IhZwvvEt1NNkis+A4xGSuGCY+ymsGnEF
Phone: encrypted-i7IPbdlBcIMdPZlYIAgXOpoxS0JnLUaL/mrtzNil9Q/jZPTa
Project: gnunet

Name: Diogo Peralta Cordeiro
Email: up201705417@gmail.com
AltEmail: encrypted-eIx36A/+Koly94HCU+771weo2q48HwnPsGfdwKFyVDi/dgT8
Phone: encrypted-zMeoz69reoJ2Y2j/zzhsdLJ+ylwcw7rNzPT87rBVlaFULKj4
Project: social

Name: Massimiliano Gubinelli
Email: m.gubinelli@gmail.com
AltEmail: encrypted-Znlk+1EYi2kZCL2Yuqfgp5EIhY1MOEtpwp522g7UkdtEWzjc
Phone: encrypted-i+spPhMGSQ2ZgCIMgkR+BRbCAUmUyw5+Pk1BOja84i1o27k5
Project: texmacs
Note: http://www.texmacs.org/tmweb/contribute/team-massimiliano.en.html

Name: Thomas Schwinge
Email: thomas@schwinge.name
AltEmail: encrypted-QDVnOzzhmOW4K1L0MG12O3/e/EtHgGJLifycFm1+lZNMPLFR
AltEmail: encrypted-femaWrE5cRvGq60EnMbIaSAeFqGMLx1fS7SYEtQJ3eQKrNvX
Phone: encrypted-FwA4uzLiJFCp8es0VLbZXMfoTi+rUPoe/EO4YWOrXe3md28v
Phone: encrypted-vbG+UrwcHZFPAJoXO2KPRxFj1w5HXQzZPjX84WkFbkfFm4Wq
Project: hurd
Project: glibc
Project: binutils
Project: gdb

Name: Zoltán Kovács
Email: zoltan@geogebra.org
AltEmail: encrypted-qQNw1QOhDKTqYcTVeMv4MN+lZNf+2Yvj2b4CQp8W+kwQ7bsB
Phone: encrypted-GIp4FUD/GpSIeyrSHWhgqNXZgvWwwTVnK02ZaFSbr7i/+sAL
Project: xaos

Name: Jose E. Marchesi
Email: jemarch@gnu.org
Project: poke

Name: Darshit Shash
Email: darnir@gnu.org
Project: wget

Name: Simon Sobisch
Email: simonsobisch@gnu.org
Project: gnucobol

Name: Rocky Bernstein
Email: rocky@gnu.org
AltEmail: encrypted-lGSoMSMRlKoecnoaOPLwIZEPSO1aMEidSBIEQ/jkTaoN1zX/
Phone: encrypted-RmV04kDmlV+977W6h7iiOvU+uIILVfghonpUk3m6+IHYnc1cedeg/5WWpc/20JQXq2EcAg==
Project: libcdio
Project: vcdimager
Project: emacs

Name: Shivam Kohli
Email: kohlishivam05@gmail.com
AltEmail: encrypted-1eAB3Y26OecBl2yC8aGG1dLlrBRr8zYhHj2woIrEnIIlkxrQ
Phone: encrypted-mizpBDcoNPc5G5DAW7+6Fwh6u5/N0V5VboMIhKq7Emb4DZ1y
Project: taler

Name: Florian Dold
Email: florian@dold.me
AltEmail: encrypted-C06DfmPuv+TXs6PMIrC0yfGSuOSCzs6KzMuigpxnO99YhhK/
Phone: encrypted-pRfCl6US3EzrxofSme3zvQaBJwSqQjQhp0wqWncT3v3Zzsrh
Project: taler

Name: Darcy Shen
Email: sadhen@zoho.com.cn
AltEmail: encrypted-SzHusRrQ0SVvFMKWrjNA9p5v08l7mJ9g1a/5vlysvsjsVy28
Phone: encrypted-QrIj29MncIDonW8yoqktM8i2PiFwR3lLgxSuQR2iTVm/DRlT
Project: texmacs

Name: Joris van der Hoeven
Email: vdhoeven@lix.polytechnique.fr
AltEmail: encrypted-YX1kHVZNx/h2iOlU5H+CBdsmTsucn7Q/uu3DUXqmnd0qfHdY
Phone: encrypted-t/rfORb2tiSuaQrRob5fZU45ijtut13CbL4OUoqNoxSm5gQQ
Project: texmacs
Note: Currently in Canada and soon without phone.

Name: Daniel Supernault
Email: danielsupernault@gmail.com
AltEmail: encrypted-rFBsSTelp/+Xpc7e9uAq83v5gFBo+EHh613ONAwaPHb08tuW
Phone: encrypted-9eIZDQahWI1k61RNbaJXmPEyALs=
Project: social

Name: Alexei Sorokin
Email: sor.alexei@meowr.ru
AltEmail: encrypted-Me73r99jWycds7hDtQxsBa2X3jWZvXnJM0yrLxqjhyfmUKGw
Project: social

Name: Werner Lemberg
Email: wl@gnu.org
AltEmail: encrypted-jo202buOrrBku4abeNG/4Uw46N8/59x8UWhTOqFrMiUz0TVB
Phone: encrypted-rE+S7NPvE9JY3coRqNJVEjMDA8GoNtL3Obxn6/+x0ti8z/BT
Project: lilypond

%rec: Student
%key: Email
%type: Email email
%type: Mentor rec Mentor
%type: Project rec Project

Name: Owen Lamb
Email: owendlamb@gmail.com
Project: lilypond
Mentor: wl@gnu.org
Idea: Adopt the SMuFL music font encoding standard
Selected: yes

Name: Brice Waegeneire
Email: brice.wge@gmail.com
Project: guix
Mentor: dannym@scratchpost.org
Idea: Booting via network
Selected: yes

Name: Zach Shaftel
Email: zshaftel@gmail.com
Project: emacs
Mentor: rocky@gnu.org
Idea: Add byte-offset information to Emacs Lisp bytecode
Selected: yes

Name: Bruno Casteleiro
Email: brvnocasteleiro@gmail.com
Project: social
Mentor: danielsupernault@gmail.com
Mentor: up201705417@gmail.com
Mentor: sor.alexei@meowr.ru
Idea: Federation in V3

Name: Mason Hock
Email: masonhock@gmail.com
Project: lilypond
Mentor: ul@openlilylib.org
Idea: Frescobaldi Extensions

Name: Alberto Flores
Email: albertoefg@gmail.com
Project: guix
Mentor: boskovits@gmail.com
Idea: syntax and semantics of systemd units in the Shepherd

Name: Sachin Singh
Email: sachinkumarsingh092@gmail.com
Project: gnuastro
Mentor: mohammad@akhlaghi.org
Idea: Image registration wiht the celestial sphere and non-linear
+ warping in Gnuastro
Selected: yes

Name: Susanna Di Vita
Email: susanna.divita.2@gmail.com
Project: social
Mentor: rozzin.gsoc@hackerposse.com
Mentor: up201705417@gmail.com
Mentor: danielsupernault@gmail.com
Idea: Implementation of Pleroma API based GNU social Frontend
+ and Pleroma API
Selected: yes

Name: Satyam Saxena
Email: satyamsaxena197@gmail.com
Project: taler
Mentor: grothoff@gnu.org
Mentor: kohlishivam05@gmail.com
Mentor: stanisci.m@gmail.com
Idea: Integrate GNU Taler payments with the Pretix ticketing system

Name: Kostasch
Email: koschasialis@gmail.com
Project: poke
Mentor: jemarch@gnu.org
Idea: Machine interface for GNU poke
Selected: yes

Name: Eliseu Amaro
Email: eliseu.amaro98@gmail.com
Project: social
Mentor: danielsupernault@gmail.com
Mentor: up201705417@gmail.com
Mentor: rozzin.gsoc@hackerposse.com
Mentor: sor.alexei@meowr.ru
Idea: New front-end classic

Name: Xiang Yue
Email: xiangyue1107@gmail.com
Project: texmacs
Mentor: m.gubinelli@gmail.com
Mentor: sadhen@zoho.com.cn
Idea: Improved TeXmacs converters
Selected: yes

Name: Anurag Aggarwal
Email: kanurag94@gmail.com
Project: xaos
Mentor: zoltan@geogebra.org
Idea: Support arbitrary precision and export high quality output
Selected: yes

Name: Hugo Sales
Email: HDCS333@gmail.com
Project: social
Mentor: up201705417@gmail.com
Mentor: sor.alexei@meowr.ru
Idea: V3 Rewrite with Symfony
Selected: yes

Name: Heng Yeow TAN
Email: TanHengYeow@gmail.com
Project: taler
Mentor: grothoff@gnu.org
Mentor: stanisci.m@gmail.com
Mentor: mschanzenbach@gmail.com
Mentor: florian@dold.me
Idea: Web-based graphical user interface for LibEuFin
Selected: yes

Name: suhasks123
Email: Suhasks123@gmail.com
Project: wget
Mentor: darnir@gnu.org
Idea: wget2 libWARC implementation and integration
