# soc2013.rec - Database for the GNU Summer Of Code.
#
#
# This file contains the list of projects/ideas and mentors that are
# part of the GNU application for SoC 2012.
#
# Types of records stored in this file:
#
#     Project -> GNU package participating in the SOC.
#     Mentor  -> Mentor for one or more projects.
#     Student -> Student participating in SOC under GNU.
#
# For inquiries about this file please contact summer-of-code@gnu.org.

%rec: Project
%key: Name
%mandatory: Description
%doc: GNU package contributing ideas for the SOC 2013.
%type: EssentialSlots,DesiredSlots int
%summary: Name

Name: classpath
Description: 
+ GNU Classpath, Essential Libraries for Java, is a GNU project to
+ create free core class libraries for use with virtual machines and
+ compilers for the java programming language.
IdeasLink: http://developer.classpath.org/mediation/GoogleSoC2013
EssentialSlots: 2
DesiredSlots: 3

Name: guile
EssentialSlots: 1
DesiredSlots: 1

Name: wget
Description: 
+ It is package for retrieving files using HTTP, HTTPS and FTP, the
+ most widely-used Internet protocols
EssentialSlots: 1
DesiredSlots: 1

Name: clisp
Description: 
+ GNU CLISP is an ANSI Common Lisp Implementation.
Contact: clisp-devel@sf.net
EssentialSlots: 0
DesiredSlots: 0

Name: emacs
Description: 
+ GNU Emacs is the extensible, customizable, self-documenting
+ real-time display editor.
EssentialSlots: 2
DesiredSlots: 2

Name: gnucap
Description: 
+ Gnucap is a modern post-spice circuit simulator with several
+ advantages over Spice derivatives.
IdeasLink: http://gnucap.org/dokuwiki/doku.php?id=gnucap:projects
Contact: Al Davis (ad201@freeelectron.net)
EssentialSlots: 1
DesiredSlots: 2

Name: gnunet
Description: 
+ GNUnet is a framework for secure peer-to-peer networking that does not
+ use any centralized or otherwise trusted services. A first service
+ implemented on top of the networking layer allows anonymous
+ censorship-resistant file-sharing. Anonymity is provided by making
+ messages originating from a peer indistinguishable from messages that
+ the peer is routing. All peers act as routers and use link-encrypted
+ connections with stable bandwidth utilization to communicate with each
+ other. GNUnet uses a simple, excess-based economic model to allocate
+ resources. Peers in GNUnet monitor each others behavior with respect
+ to resource usage; peers that contribute to the network are rewarded
+ with better service. GNUnet is part of the GNU project.
IdeasLink: https://gnunet.org/gsoc2013
Contact: {bart, wachs}@net.in.tum.de
EssentialSlots: 2
DesiredSlots: 3

Name: guix
Description: 
+ GNU Guix is a purely functional package manager, and associated free
+ software distribution, for the GNU system.  In addition to standard
+ package management features, Guix supports transactional upgrades and
+ roll-backs, unprivileged package management, per-user profiles, and
+ garbage collection.
Contact: bug-guix@gnu.org
EssentialSlots: 1
DesiredSlots: 1

Name: hurd
Description: 
+ The GNU Hurd is the GNU project's replacement for the Unix kernel.  It
+ is a collection of servers that run on the Mach microkernel to
+ implement file systems, network protocols, file access control, and
+ other features that are implemented by the Unix kernel or similar
+ kernels (such as Linux).
IdeasLink: http://www.gnu.org/software/hurd/community/gsoc/project_ideas.html
Contact: bug-hurd@gnu.org
EssentialSlots: 1
DesiredSlots: 3

Name: libredwg
Description: 
+ GNU LibreDWG is a free C library to handle DWG files. It aims to be a
+ free replacement for the OpenDWG libraries. DWG is the native file
+ format of AutoCAD.
IdeasLink: http://libreplanet.org/wiki/LibreDWG/SummerOfCode">http://libreplanet.org/wiki/LibreDWG/SummerOfCode
Contact: libredwg@gnu.org
EssentialSlots: 2
DesiredSlots: 2

Name: mediagoblin
Description: 
+ GNU Mediagoblin is a free software media publishing software that
+ everyone can use.
IdeasLink: http://wiki.mediagoblin.org/GSOC_2013">http://wiki.mediagoblin.org/GSOC_2013
Contact: devel@mediagoblin.org
EssentialSlots: 2
DesiredSlots: 3

Name: octave
Description: 
+ GNU Octave is a high-level interpreted language, primarily intended
+ for numerical computations. It provides capabilities for the numerical
+ solution of linear and nonlinear problems, and for performing other
+ numerical experiments. It also provides extensive graphics
+ capabilities for data visualization and manipulation. Octave is
+ normally used through its interactive command line interface, but it
+ can also be used to write non-interactive programs. The Octave
+ language is quite similar to Matlab so that most programs are easily
+ portable.
IdeasLink: http://wiki.octave.org/Summer_of_Code_Project_Ideas
Contact: maintainers@octave.org
EssentialSlots: 3
DesiredSlots: 8

Name: prologforjava
Description: 
+ GNU Prolog for Java is an implementation of ISO Prolog as a Java
+ library (gnu.prolog). Last year a successful GSoC student revived it
+ and made it compliant with part 1 of the ISO standard.
Contact: info-gnuprologjava@gnu.org
EssentialSlots: 1
DesiredSlots: 1

Name: recutils
Description: 
+ GNU Recutils is a set of tools and libraries to access
+ human-editable, plain text databases called recfiles.  The data
+ is stored as a sequence of records, each record containing an
+ arbitrary number of named fields.
Contact: bug-recutils@gnu.org
EssentialSlots: 1
DesiredSlots: 1

Name: bison
Description: 
+ A general-purpose parser generator that converts a grammar description
+ for an LALR context-free grammar into a C program to parse that
+ grammar.
Contact: bug-bison@gnu.org
EssentialSlots: 1
DesiredSlots: 1

Name: gnustep
Description: 
+ The purpose of this project is to create a free and open version of
+ the Cocoa (formerly known as NeXTSTEP/OpenStep) APIs and tools for as
+ many platforms as possible.
+ 
+ GNUstep provides a robust implementation of the AppKit and Foundation
+ libraries as well as the development tools available on Cocoa,
+ including Gorm (the InterfaceBuilder) and ProjectCenter
+ (ProjectBuilder/Xcode). GNUstep currently supports Unix (GNU/Linux and
+ GNU/HURD, Solaris, NetBSD, OpenBSD, FreeBSD, Darwin) and Windows.
EssentialSlots: 1
DesiredSlots: 1

%rec: Mentor
%key: Id
%mandatory: Project
%type: Project rec Project
%confidential: Phone AltEmail

Id: pitanga
Name: Rodrigo Rodriguez Dasilva
Project: libredwg

Id: dasrew
Name: Doug Stewart
Project: octave

Id: civodul
Name: Ludovic Courtes
Project: guix

Id: akim
Name: Akim Demaille
Project: bison
Email: akim@lrde.epita.fr
AltEmail: encrypted-rXlcAwT4rPWwmjiCdOvjVCk1oL11k4P17WMKNRUnW1hDDOpN
Phone: encrypted-6qL/3rY5rWvceaH22ofo0zhSa78ts9HksmX+3vwS/7pVjSZ8

Id: Joakim Verona
Email: joakim@verona.se
Phone: encrypted-SVqaSN6gKkezPEaeZNzYFrREbjE=
Project: emacs
Freenode: jave

Id: bartpolot
Name: Bart Polot
Project: gnunet

Id: cdf
Name: Carlo De Falco
Project: octave

Id: aeva
Name: Aeva Palecek
Project: mediagoblin

Id: spaetz
Name: Sebastian Spaeth
Project: mediagoblin

Id: monnier
Name: Stefan Monnier
Project: emacs

Id: aaronw
Name: Aaron Williamson
Project: mediagoblin

Id: joar
Name: Joar Wandborg
Project: mediagoblin

Id: nathanyergler
Name: Nathan Yergler
Project: mediagoblin

Id: cwebber
Name: Christopher Webber
Project: mediagoblin

Id: drt24
Name: Daniel Thomas
Project: prologforjava

Id: fhgpqf8h
Name: Al Davis
Project: gnucap

Id: fred_gnustep
Name: Fred Kiefer
Project: gnustep

Id: giuseppe
Name: Giuseppe Scrivano
Phone: encrypted-EV9ZCCrcS88Ezh4f0dyEPuP7UGQ=
Project: wget

Id: gnu_andrew
Name: Andrew John Hugues
Email: gnu_andrew@member.fsf.org
AltEmail: encrypted-yjyEzpTEIjS7iFCQphfZ2EYRM7N50AjabrOXEulp+tbkUYiD
Phone: encrypted-A7wtvFApPGfYE7KJd5Ett0QKWyqEKya28L1N3oXrS+L9kYYk
Project: classpath

Id: goffioul
Name: Michael Goffioul
Project: octave

Id: grothoff
Name: Christian Grothoff
Project: gnunet

Id: hsrai
Name: Hardeep Singh Rai
Email: dotorgdotcom@gmail.com
AltEmail: encrypted-46VJjeN8296CQeCPpcGTbR6f3v2hNezNVQaegP/nFVc8hu46
Phone: encrypted-8YGidvG+4VFHWP4y8QAuWY1RuYNGbeUP+XlkapSaN2TDmv+f
Project: libredwg

Id: jordigh
Name: Jordi Gutierrez Hermoso
Project: octave

Id: jwe
Name: John W. Eaton
Project: octave

Id: mtmiller
Name: Mike Miller
Project: octave

Id: mwachs
Name: Matthias Wachs
Project: gnunet

Id: ndr
Name: Nils Durner
Project: gnunet

Id: neugens
Name: Mario Torre
Email: neugens.limasoftware@gmail.com
Phone: encrypted-qkTBatZZAA9C/WGxdsf5sq88VXoVqF+yFJSZooSACUT9I3Xky62XDNvYmzPv3RlzNbgT+Q==
Phone: encrypted-3ImOl/iKfZeSWs/H0EDWWd3sFjPPA5oqVBMVmQGT0YJKtBofvWCZ8c0T6hqyCU3kL9gmIw==
Project: classpath

Id: rkennke
Name: Roman Kennken
Email: roman@kennke.org
AltEmail: encrypted-RSFyAzfn+A1xjJ+RH6r5vtz6VinwqIII6eN8FJNecA8oTybe
AltEmail: encrypted-CUJ64GzHPkKfvPwmKHCoDInr7YJyWp7hJDzM6y2I9HPaLBfB
Phone: encrypted-uB/5xgbye80Srfe4Rno2XUi/dZ/uADOX2la+Q3ero9ca7buI
Project: classpath

Id: steap
Project: guix

Id: nirkrakauer
Name: Nir Krakauer
Project: octave

Id: pnoffke
Name: Patrick Noffke
Project: octave

Id: prolog2java
Name: Pavan Gorakavi
Email: gorakavi@gmail.com
Project: prologforjava

Id: sds
Name: Sam Steingold
Project: clisp

Id: sthibaul
Name: Samuel Thibault
Project: hurd

Id: theraven
Name: David Chisnall
Email: David.Chisnall@cl.cam.ac.uk
AltEmail: encrypted-i+m/QkbSoEUNc5aerM6yhnMbNfKFvqYMw0s1sIgYek+FvW9u
Phone: encrypted-nDbqaegnnzQWLXnC8OjRzSaTAMY1d0Pe5AJorpkKTPN3KQFA
Project: gnustep

Id: tschwinge
Name: Thomas Schwinge
Email: tschwinge@gnu.org
AltEmail: encrypted-7lUlD7Y5/6bEWLL5i/YkHlBgmxb6hmMcJzJNddSfkQB4dtSh
AltEmail: encrypted-+u24GKI4sCYQywtWpfOUBGVj2PtmWVLDjCv6EKAkoaX34WTK
Phone: encrypted-ElMgBNtW7yquthEOScKAItAzw/lcCEMRt0vRoI8IGdvjV0dR
Project: hurd

Id: jemarch
Name: Jose E. Marchesi
Email: jemarch@gnu.org
Phone: encrypted-Q3XRV5lRcf+q5s4shlWwX2V2T0CCcFXBTfw3yZu791npYhFT
Project: recutils

%rec: Student
%key: Email
%mandatory: Work
%type: Email email
%type: Mentor rec Mentor
%type: Project rec Project

Name: Matheus Vieira Portela
Email: matheus.v.portela@gmail.com
Work: Octave - Create a graphical design tool for tuning closed loop control system (control pkg)
Project: octave
Mentor: dasrew

Email: elisa.deugenio@gmail.com
Work: New Gtk3 Look and Feel for OpenJDK
Project: classpath
Mentor: neugens

Email: hacklu.newborn@gmail.com
Work: Improve the GDB Port for GNU Hurd
Project: hurd
Mentor: tschwinge

Email: daimrod@gmail.com
Name: Grégoire Jadi
Work: Emacs XWidgets
Project: emacs
Mentor: Joakim Verona

Email: ewing.rj@gmail.com
Work: Mediagoblin Pluggable User Authentication & Implementations
Project: mediagoblin
Mentor: aaronw

Name: Marco
Email: marcovass89@hotmail.it
Work: Octave Numerical Project - General purpose Finite Element library
Project: octave
Mentor: cdf

Name: Raipeng
Email: rli@cs.umn.edu
Work: Incomplete sparse factorizations ichol, ilu in Octave
Project: octave
Mentor: cdf

Name: Andrej Lojdl
Email: andrej.lojdl@gmail.com
Work: Octave - Tex/LaTeX markup
Project: octave
Mentor: pnoffke

Name: Juan Manuel Perez Rua
Email: juanmanpr@gmail.com
Work: Octave - Color management functions in image package
Project: octave
Mentor: pnoffke

Name: Gaganjyot
Email: thegaganx@gmail.com
Work: API for LibreDWG
Project: libredwg
Mentor: pitanga

Name: Avneet
Email: kauravneet958@gmail.com
Work: Decode Refactoring
Project: libredwg
Mentor: hsrai

Name: Albert Flaig
Email: a.flaig@gmail.com
Work: Caciocavallo Jenkins-Plugin and Merging to OpenJDK
Project: classpath
Mentor: rkennke

Name: Farshad Muhammad
Email: 
Work: Port the GTK+ AWT support for GTK3
Project: classpath
Mentor: gnu_andrew

Name: Aditi Mittal
Email: aditi.iitr@gmail.com
Work: Blogging System for MediaGoblin
Project: mediagoblin
Mentor: spaetz

Name: Praveen Kumar
Email: praveen97uma@gmail.com
Work: Search Interface for MediaGoblin
Project: mediagoblin
Mentor: nathanyergler
Mentor: cwebber

Name: Ahsan Shahid
Email: ahsan.ali.shahid@gmail.com
Work: The "Agora" website
Project: octave
Mentor: jordigh

Name: Carnë Draug
Email: carandraug+dev@gmail.com
Work: Octave - image processing of ND images
Project: octave
Mentor: jordigh

Name: Vyatautas Jancauskas
Email: unaudio@gmail.com
Work: Octave - fix audio processing
Project: octave
Mentor: mtmiller

Name: Kai Torben Ohlhus
Email: k.ohlhus@gmail.com
Work: GNU Octave - Incomplete sparse factorizations ichol, ilu
Project: octave
Mentor: nirkrakauer

# Name: Jacek Świerk
# Email: jacekswierk@gmail.com
# Work: Various missing algebraical functions implementations for GNU Octave,
# Project: octave
# Mentor: cdf

Name: Ivan Vučica
Email: ivan@vucica.net
Work: GNUstep: Opal-backed rendering system for gnustep-gui in gnustep-back
Project: gnustep
Mentor: theraven
Mentor: fred_gnustep

Name: Rishabh Garg
Email: rishabhgarg.as@gmail.como
Work: Prolog For Java - Rewrite The Parser
Project: prologforjava
Mentor: drt24

# Email: darrazo16@gmail.com
# Work: Octave Numerical Project
# Project: octave
# Mentor: cdf

# Name: Laura Ferrarotti
# Email: l.ferrarotti@gmail.com
# Work: GNU Octave: Numerical. Incomplete sparse factorizations ichol,
# + ilu. Various sparse matrix improvements. Implement solver for
# + initial-boundary value problems for parabolic-elliptic PDEs in 1D.
# Project: octave
# Mentor: cdf

Name: LYH
Email: LYH.Kernel@gmail.com
Mentor: jwe
Project: octave

Name: Xue Fuqiao
Email: xfq.free@gmail.com
Work: Emacs - VC Modernization
Project: emacs
Mentor: monnier

Name: Harsha
Email: totakura@in.tum.de
Work: [GNUnet] Improvement of the GNUnet Installer/updater
Project: gnunet
Mentor: grothoff

Name: Claudiu Olteanu
Email: olteanu.vasilica.claudiu@gmail.com
Work: Improving GNUnet's adhoc connectivity with a bluetooth transport
Project: gnunet
Mentor: bartpolot
Mentor: mwachs

Email: tg-soc@tgbit.net
Work: Integrating GNUnet and PSYC
Project: gnunet
Mentor: bartpolot
Mentor: grothoff

Name: Omar Tarabai
Email: tarabai@devegypt.com
Work: GNUnet - Creation of a GUI for the GNUnet PlanetLab/Parallel Deployment tool
Project: gnunet
Mentor: grothoff
Mentor: mwachs

Name: Raghunandan Mudadla
Email: mudadlaraghunandan567@gmail.com
Work: Language Plugin for Qucs
Project: gnucap
Mentor: fhgpqf8h

Name: Rishabh Yadav
Email: rishabh.yadav.ece11@iitbhu.ac.in
Work: Gnucap
Project: gnucap
Mentor: fhgpqf8h

Name: TOLMER Valentin
Email: valentin.tolmer@gmail.com
Work: Bison - partial orders precedence and associativity
Project: bison
Mentor: akim

Name: Darshit Shah
Email: darnir@gmail.com
Work: Wget: Move Test Bench Suite from Perl to Python.
Project: wget
Mentor: giuseppe

Name: BT Templeton
Email: bt@hcoop.net
Work: Replace the Elisp interpreter with Guile's Elisp compiler
Project: guile
Mentor: civodul

Name: Maninya
Email: maninya@gmail.com
Work: Recutils - Add Python bindings to Recutils
Project: recutils
Mentor: jemarch

# End of soc2013.rec
