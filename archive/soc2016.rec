# soc2015.rec - Database for the GNU Summer Of Code.
#
#
# This file contains the list of projects/ideas and mentors that are
# part of the GNU application for SoC 2015.
#
# Types of records stored in this file:
#
#     Project -> GNU package participating in the SOC.
#     Mentor  -> Mentor for one or more projects.
#     Student -> Student participating in SOC under GNU.
#
# For inquiries about this file please contact summer-of-code@gnu.org.

%rec: Project
%key: Name
%mandatory: Description
%doc: GNU package contributing ideas for the SOC 2014.
%type: EssentialSlots,DesiredSlots int
%summary: Name
%type: When date
%type: Status enum FOO BAR BAZ

Name: Hurd
EssentialSlots: 2
DesiredSlots: 2

Name: clisp
EssentialSlots: 1
DesiredSlots: 1

Name: Libffcall
EssentialSlots: 2
DesiredSlots: 2

Name: Texinfo
EssentialSlots: 1
DesiredSlots: 1

Name: LibertyEiffel
EssentialSlots: 2
DesiredSlots: 2

Name: GNUnet
EssentialSlots: 2
DesiredSlots: 3

Name: MediaGoblin
EssentialSlots: 1
DesiredSlots: 1

Name: GCC
EssentialSlots: 3
DesiredSlots: 3

Name: Lilypond
EssentialSlots: 4
DesiredSlots: 4

Name: Guix
EssentialSlots: 4
DesiredSlots: 6

Name: Guile
EssentialSlots: 0
DesiredSlots: 1


%rec: Mentor
%key: Id
%mandatory: Project
%type: Project rec Project
%confidential: Phone AltEmail

Id: neugens
Name: Mario Torre
Email: neugens.limasoftware@gmail.com
AltEmail: encrypted-id8isPnK8xnGZ4zj4lBuQlygZ5s=
Phone: encrypted-q17Hlj6m2DSlFcLi499H6JPlVmnA102XW4FK0KUIzyjAaP8x
Project: GNU Project (GNU Classpath)

Id: teythoon
Name: Justus Winter
Email: justus@gnupg.org
AltEmail: encrypted-LmtYL1PQ6u5aNDLk3q8ziiKs0LW2H1v0rPMsuxfu8HASymd2
Phone: encrypted-f1Iv+JvPY9dO9am7vipLAnpJairji5I4bML2pD8C6X7O2ggh
Project: Hurd, GUIX

Id: jrh
Name: Jamison Hope
Email: jrh@theptrgroup.com
AltEmail: encrypted-+Ie8HxdNP7u5LTAkC661CMfkpNRhkVvM0AACUFfjsYxPYWfS
Phone: encrypted-yCv066tviCa0pafOnGlTjsmS6vg=
Project: Kawa

Id: per_bothner
Name: Per Bothner
Email: per@bothner.com
AltEmail: encrypted-MoJQL5H/rGVvPndtvc77vzIgHDs=
Phone: encrypted-nSji1Vu7JQd8Z0sU8lnpNnct4qIK03EJKB90DPYY9pxRAmpJ
Project: Kawa and TexInfo

id: gavin
Name: Gavin Smith
Email: GavinSmith0123@gmail.com
AltEmail: encrypted-4Dsaje5k96W1rwqYMBKMo0mPaVej/sOBUKzQO88/hjKY0qG3
Phone: encrypted-tUViq44eAmHnURipfHo2dbdllpOJHdGO0AMl4g0vdh0G5+S2
Project: Texinfo

id: breton
Name: Boris Bobrov
Email: breton@cynicmansion.ru
AltEmail: encrypted-4tToCehd2k1R4fJ23D8AFuqYACc=
Phone: encrypted-x6ZUV+FJRgl919fINPREF+j8Kq5xFhzu9DFW0G7/i/3cEoRJ
Project: MediaGoblin

id: ramack
Name: Raphael Mack
Email: g@raphael-mack.de
AltEmail: encrypted-MsTO6JwW5wHxSXwFCIC2IW2cZ/b7b2yGvsalF6tR1ng0L4oa8EZBsr1go5voxLs3Mmv7vf70g9qgqJOi9A9FHgR02HA=
Phone: encrypted-MJGQC3ADy8mdRD2t9MQqjFMudGBOOuMFiwDliztUr6WiZOPD
Project: LibertyEiffel

Id: tybor
Name: Paolo Redaelli
Email: paolo.redaelli@gmail.com   
AltEmail: encrypted-4PFp1gTEXw28AG3t24/CENUdeHVPV+nJWwKFNN+PdXtaYZ8O
Phone: encrypted-a3VrPe2npVtexoVJoetPrdYqKuT3cvJr9Dj0o0s/fROyoq+I
Project: LibertyEiffel

Id: ndurner
Name: Nils Durner
Email: durner@gnunet.org
AltEmail: encrypted-xDHLZ1wYniZAbQnMJUO1Q135ApUw6b1ERIK8uZs26ERvQZME
Phone: encrypted-iGvBGBsStgJzO+cCSydOWSku2aWvVzomXhsk0cOX3K9g1eG0
Project: GNUnet, Taler

Id: ludovic
Name: Ludovic Courtès
Email: ludo@gnu.org
AltEmail: encrypted-OkWbz3XIPKjadTda8O3M4TBBIWep3PsCafdKttyvPLyDctum
Phone: encrypted-rlnGe3N30eeTtrDUwQqlCrSbCQXq6vkc7wZP+aZBA9l9jaZX
Project: guix
Project: guile
Project: shepherd

Id: sthibault
Name: Samuel Thibault
Email: samuel.thibault@gnu.org
AltEmail: encrypted-e6PkPyMj5rwWlyqblLddw1uS7RWhX42znvUrWIWswfcM4TIu
Phone: encrypted-BEnwER1K0swy1EKT/5QumCcIFs0UowBH4PjbPuTsazoHdC20
Project: Hurd

Id: rbraun
Name: Richard Braun
Email: rbraun@sceen.net
AltEmail: encrypted-SqlSN5XVBeRUtyIGJDqAWAgseooD18aOY2ZUW49rnl7iQreK
Phone: encrypted-k6219r1e1OyDab7nMG+cI6aOUyxfb5Vce/kc5eQoXn5ayP8Q
Project: GNU/Hurd

Id: cwebber
Name: Christopher Allan Webber
Email: cwebber@dustycloud.org
AltEmail: encrypted-QXomARF7fmVx86d7KnS7iIzKJhUwI9Wi6VIBzgYs64VpY69c
Phone: encrypted-0XwJls4ohMFS7rXF0GKZMuGpOytARbzRRDkdKOTQjXe66f8f
Project: MediaGoblin

Id: grothoff
Name: Christian Grothoff
Email: christian.grothoff@inria.fr
AltEmail: encrypted-3Ua4VlbUroGylRWSMpryE0Dn+SKgGYWDzhffPDiQCprxH8u3
Phone: encrypted-PBb+2OJCSfWk0Ob72nJaJlq7q9F3ctOxjRIKW1R8EIkTJkS3
Project: GNUnet, GNU Taler, GNU libmicrohttpd, GNU libextractor

Id: burdges
Name: Jeff Burdges
Email: jeffrey.burdges@inria.fr
AltEmail: encrypted-ozwh8KmVJVA5cUMkaOkZPY7F/CTCR+1MwbEQVEKlrZyNLtAf
Phone: encrypted-JmHflOgkPRklQ9/sp1w4pvL+aliC6DkAaTDN9IBBxfHLAReU
Project: GNUnet, GNU Taler

Id: martin
Name: Martin Schanzenbach
Email: mschanzenbach@posteo.de
AltEmail: encrypted-FEF76j4K9H8ARUplW6ULoowVnSw=
Phone: encrypted-7yc2/wUp0ah3Mt+ftKrZEoEU9VVLGcfZZlpRdrHANMGfft9T
Project: GNUnet

Id: marcello
Name: Marcello Stanisci
Email: marcello.stanisci@inria.fr
AltEmail: encrypted-GeAiMXFvGf3j4nst9WyElXVvVHA=
Phone: encrypted-uFNfViESpDNk/afHhHfwaycP3kdirccVUi8RGnT35dqeXI7i
Project: GNU Taler

Id: kaur
Name: Avneet Kaur
Email: kauravneet958@gmail.com
AltEmail: encrypted-lFi39vCITxEtE/UhfC/ClliaDc8=
Phone: encrypted-YeoM/6D5CAB8w7eGP/cjLBVjcuPY58XQ1V1BgSrvukXWxBY1
Project: Libffcall

Id: jalin
Name: Jishnu Jalin
Email: jishnujalin@gmail.com
Phone: encrypted-WzbxA31g4mHtR/3bugo+5WHYb9mVmcYL/EVhHOtEtGChABFs
Project: Libffcall

Id: sds
Name: Sam Steingold
Email: sds@gnu.org
AltEmail: encrypted-AOMFHXmCkoBQ+1m62dNT8C3lKQll9TIFe6lWa3s2OH1Fngwy
Phone: encrypted-vp7WuZlnCxCHiRBBpzivwffrlrE1P4lBg6qGduUCncQk63iu
Project: GNU CLISP

Id: cadrian
Name: Cyril Adrian
Email: cyril.adrian@gmail.com
AltEmail: encrypted-Bw/tGj6PdX+g9Uy+nhEDhHIrDuU=
Phone: encrypted-kt+FwbzXU5sPYXpczJB8EAbwD/0GSfD5LgEcshU8TsipChyP
Project: LibertyEiffel

Id: fhgpqf8h
Name: Al Davis
Email: ad201@freeelectron.net
AltEmail: encrypted-fb9N7Xz4kbUksN9B3E2/byD+ft/dhhsJsYWZwfrwnfY5jhZ/
AltEmail: encrypted-C5N/oVUs3ScX15RcIC7JlA0MEKiXTwF4r8oDnLSij+bNSMCL
Phone: encrypted-xqa/EfVqAdE3Vkgog8lh6xvIfQFg1A6mhVIH9fYUl0nzuD/H
Project: Gnucap

Name: Carl Sorensen
Email: carl.d.sorensen@gmail.com
AltEmail: encrypted-jhgB1PBqDinIR9zUV1X5dtLUR6qVMwkCpdnmYyUTZJLiF1h+
Phone: encrypted-YH1Jtc4MFGadGJSSi8m3W+wetdfG1fRLG1hLn9K1WNKPlmvR
Project: GNU LilyPond

Name: Urs Liska
Email: ul@openlilylib.org
AltEmail: encrypted-45MfSgwJ/JSBQHohrYboY6IGcs2QWjcColDVlQsjgmx5CNrj
Phone: encrypted-OUQhmhPWcKRchN3I28wXJ/z92wuYS9tqQjuj0BIkixeBcXj2
Project: GNU LilyPond

Name: Thomas Schwinge
Email: thomas@schwinge.name
AltEmail: encrypted-owYnsnBMWfsBuNpgBZ5b54YBGrkezstp8MFyBQ0Io//tDGS9t4CdOiSW+696OMFwv1HAHlulgoeWcaBOlaB4OUfYWIl5YQO3MsBYzmUqEhKnZtnqnOOMD3pps09ztmTp0fI/pCNIx9jjmK7QGLZ/q5+dc9jwCfHPMyolomHYIviaCJo3
Phone: encrypted-vRFXt+twDuRVakWLlNI0MSKXqZcH1mE5oMGTL9n7LoFPyID9MLFsp+W1pailTIBU9Vj1IpdJnBqs0JkQK8celFi/9G4=
Project: Hurd, GCC

Id: davexunit
Name: David Thompson
Email: davet@gnu.org
AltEmail: encrypted-lhqIm/wAy9s/dNFdhFHU7Nq2fwb3vI2tAsko1xrS/wDIhoFC
Phone: encrypted-NrkPTteIKrFlLBjN8suJdzXpmxSDijDWoNF4yLSBUKsqGGjn
Project: Guix

Name: Manuel Lopez-Ibanez
Email: lopezibanez@gmail.com
AltEmail: encrypted-81dTDXAEwTlZJPf4VSdwjoOC3Drhr7aCff2/p2eVulg2mL4D
Phone: encrypted-d78qKY0FM9t5nm+mSQrw6sy1FVRvKMbSLIcyAdHwILclnUpO
Project: GCC

Name: Jan-Peter Voigt
Email: jp.voigt@gmx.de
Phone: encrypted-3EEmnRSPI1jEYVs1wUd6uFSas3lNsHqlYQVqwKky64NcsKK8
Project: LilyPond

Name: Werner Lemberg
Email: wl@gnu.org
Phone: encrypted-NoqM5Qu3yJWJlnaMVsxX+LabT3tEorAW+1+CgRMO5NcaPlc+
Project: LilyPond

Name: Gabor Toth
Email: *@tg-x.net
AltEmail: encrypted-FVoD4QvucfywJurHoeEYRbaXKDU=
Phone: encrypted-7zD+rhBzdubVRuWI9kZCn7PpiE7Efy48m2gt/pXUkYoctqXl
Project: GNUnet

Name: Jan Hubicka
Email: honza.hubicka@gmail.com
AltEmail: encrypted-8EKGB2v/pwSotRNiV9mkeLiDtiyX47TUx0BxDOW37TeLc8wC
Phone: encrypted-Wz8RZnPxKsUpRrEJYJtmrGZObi1J5zItNXyB9OBbhT9PkYS9
Project: GCC

Name: Richard Biene
Email: rguenther@suse.de
AltEmail: encrypted-pr3l/alujU5zU2CvNfkgCVgR2pMw95uZpT42Y4krEFz6r0C1
Phone: encrypted-tjtF7BToGzDhzU2J4FEqH93qgGhT67FzAE+CZgk5F9iVvIxm
Project: GCC

Name: Oleg Endo
Email: oleg.endo@t-online.de
AltEmail: encrypted-7TjnsY2V/brakvnCJ9tqMqGZZ9k=
Phone: encrypted-/5KvVJtgNGlJk8KLz5XXuW2BpLeEVNFLJ1bCMM+qfL/neexL
Project: GCC

Id: davexunit
Name: David Thompson
Email: davet@gnu.org
AltEmail: encrypted-H0EDyLfsYPSyBLSs8Vyc6/MK91RK/fPXCOkBfg7EYsCVs3WG
Phone: encrypted-TZd0FfJqaXDbcF0eVbuao8O9NsA=
Project: Guix

Name: Leo Famulari
Email: leo@famulari.name
AltEmail: encrypted-ukI/WoIJ1R0GIvcIYQuSS8tYogoSuqwNtd+Ob3ePfN3uTnvS
Phone: encrypted-BUqTPyLwzucTwxKpb9mYjfIyz7ynW3k9RvcfOzn8XdB7q98r
Project: GNU Guix

Name: Eric Bavier
Email: ericbavier@openmailbox.org
AltEmail: encrypted-ctKW2EZ8FZrX/xfR/LyjlYwqMJqx5xdMHFGGueAGFHpPVz0a
Phone: encrypted-VEz2lNt7a/qvYxMhtF2cr4afN3HvjmZ3m4HkEN+fnVXfeF4u
Project: Guix

%rec: Student
%key: Email
%type: Email email
%type: Mentor rec Mentor
%type: Project rec Project
%confidential: Name Email URL
